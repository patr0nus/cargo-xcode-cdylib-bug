# Demonstrating a re-building bug of `cargo-xcode`  

1. Open, build and run `cargo-xcode-cdylib-bug.xcodeproj`. It prints "Hello from rust" which comes from `rust_hello/src/lib.rs`

2. Change "Hello from rust" in `lib.rs` to some other string. Click the Run button in Xcode. **It still prints the old string "Hello from rust".**

3. Click the Run button again. Now it correctly prints the changed string.
