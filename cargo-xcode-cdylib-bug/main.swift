import Darwin
import Foundation

let dylibPath = NSString.path(withComponents: [
    Bundle.main.privateFrameworksPath!,
    "rust_hello.dylib"
])

let dylibHandle = dlopen(dylibPath, RTLD_NOW)!

let rustHelloAddr = dlsym(dylibHandle, "rust_hello")!
let rustHello = unsafeBitCast(rustHelloAddr, to: (@convention(c) () -> Void).self)

rustHello()
